# AbpMysqlBulkSample
Mysql bulk operation Sample in abp framework.

## References
[EFCore.BulkExtensions](https://github.com/yang-er/efcore-ext)

[Abp Framework](https://github.com/abpframework/abp)

## use
1、download this sample code 

2、change default connectionstring in appsettings.json(both in MyComp.BulkTest.DbMigrator and MyComp.BulkTest.HttpApi.Host project)

3、build solution in vs2022

4、run MyComp.BulkTest.DbMigrator project to generate database

5、run MyComp.BulkTest.HttpApi.Host project

6、open https://localhost:44347/swagger/index.html in browser，run bulk method in SampleController

