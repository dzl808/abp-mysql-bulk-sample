﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Ui.Branding;

namespace MyComp.BulkTest;

[Dependency(ReplaceServices = true)]
public class BulkTestBrandingProvider : DefaultBrandingProvider
{
    public override string AppName => "BulkTest";
}
