﻿using Volo.Abp.Localization;

namespace MyComp.BulkTest.Localization;

[LocalizationResourceName("BulkTest")]
public class BulkTestResource
{

}
