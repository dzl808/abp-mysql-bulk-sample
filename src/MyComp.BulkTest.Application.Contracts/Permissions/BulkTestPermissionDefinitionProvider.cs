﻿using MyComp.BulkTest.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace MyComp.BulkTest.Permissions;

public class BulkTestPermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var myGroup = context.AddGroup(BulkTestPermissions.GroupName);
        //Define your own permissions here. Example:
        //myGroup.AddPermission(BulkTestPermissions.MyPermission1, L("Permission:MyPermission1"));
    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<BulkTestResource>(name);
    }
}
