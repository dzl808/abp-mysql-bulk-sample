﻿using AutoMapper;

namespace MyComp.BulkTest;

public class BulkTestApplicationAutoMapperProfile : Profile
{
    public BulkTestApplicationAutoMapperProfile()
    {
        /* You can configure your AutoMapper mapping configuration here.
         * Alternatively, you can split your mapping configurations
         * into multiple profile classes for a better organization. */
    }
}
