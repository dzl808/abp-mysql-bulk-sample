﻿using System;
using System.Collections.Generic;
using System.Text;
using MyComp.BulkTest.Localization;
using Volo.Abp.Application.Services;

namespace MyComp.BulkTest;

/* Inherit your application services from this class.
 */
public abstract class BulkTestAppService : ApplicationService
{
    protected BulkTestAppService()
    {
        LocalizationResource = typeof(BulkTestResource);
    }
}
