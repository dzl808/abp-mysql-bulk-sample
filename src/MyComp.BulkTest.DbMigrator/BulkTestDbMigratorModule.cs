﻿using MyComp.BulkTest.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace MyComp.BulkTest.DbMigrator;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(BulkTestEntityFrameworkCoreModule),
    typeof(BulkTestApplicationContractsModule)
    )]
public class BulkTestDbMigratorModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
    }
}
