﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace MyComp.BulkTest.Data;

/* This is used if database provider does't define
 * IBulkTestDbSchemaMigrator implementation.
 */
public class NullBulkTestDbSchemaMigrator : IBulkTestDbSchemaMigrator, ITransientDependency
{
    public Task MigrateAsync()
    {
        return Task.CompletedTask;
    }
}
