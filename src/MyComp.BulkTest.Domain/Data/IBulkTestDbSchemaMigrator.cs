﻿using System.Threading.Tasks;

namespace MyComp.BulkTest.Data;

public interface IBulkTestDbSchemaMigrator
{
    Task MigrateAsync();
}
