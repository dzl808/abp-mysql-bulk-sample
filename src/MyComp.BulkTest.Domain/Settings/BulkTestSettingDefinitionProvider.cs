﻿using Volo.Abp.Settings;

namespace MyComp.BulkTest.Settings;

public class BulkTestSettingDefinitionProvider : SettingDefinitionProvider
{
    public override void Define(ISettingDefinitionContext context)
    {
        //Define your own settings here. Example:
        //context.Add(new SettingDefinition(BulkTestSettings.MySetting1));
    }
}
