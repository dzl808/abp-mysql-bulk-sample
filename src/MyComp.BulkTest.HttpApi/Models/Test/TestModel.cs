﻿using System;

namespace MyComp.BulkTest.Models.Test;

public class TestModel
{
    public string Name { get; set; }

    public DateTime BirthDate { get; set; }
}
