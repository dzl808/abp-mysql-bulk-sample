﻿using MyComp.BulkTest.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace MyComp.BulkTest.Controllers;

/* Inherit your controllers from this class.
 */
public abstract class BulkTestController : AbpControllerBase
{
    protected BulkTestController()
    {
        LocalizationResource = typeof(BulkTestResource);
    }
}
