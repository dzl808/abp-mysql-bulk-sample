﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MyComp.BulkTest.Data;
using Volo.Abp.DependencyInjection;

namespace MyComp.BulkTest.EntityFrameworkCore;

public class EntityFrameworkCoreBulkTestDbSchemaMigrator
    : IBulkTestDbSchemaMigrator, ITransientDependency
{
    private readonly IServiceProvider _serviceProvider;

    public EntityFrameworkCoreBulkTestDbSchemaMigrator(
        IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public async Task MigrateAsync()
    {
        /* We intentionally resolving the BulkTestDbContext
         * from IServiceProvider (instead of directly injecting it)
         * to properly get the connection string of the current tenant in the
         * current scope.
         */

        await _serviceProvider
            .GetRequiredService<BulkTestDbContext>()
            .Database
            .MigrateAsync();
    }
}
