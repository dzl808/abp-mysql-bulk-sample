﻿using MyComp.BulkTest.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace MyComp.BulkTest;

[DependsOn(
    typeof(BulkTestEntityFrameworkCoreTestModule)
    )]
public class BulkTestDomainTestModule : AbpModule
{

}
