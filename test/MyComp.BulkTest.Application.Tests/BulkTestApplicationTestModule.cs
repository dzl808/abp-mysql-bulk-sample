﻿using Volo.Abp.Modularity;

namespace MyComp.BulkTest;

[DependsOn(
    typeof(BulkTestApplicationModule),
    typeof(BulkTestDomainTestModule)
    )]
public class BulkTestApplicationTestModule : AbpModule
{

}
