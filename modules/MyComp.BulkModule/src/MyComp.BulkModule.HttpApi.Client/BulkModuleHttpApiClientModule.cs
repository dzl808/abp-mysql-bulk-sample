﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;

namespace MyComp.BulkModule;

[DependsOn(
    typeof(BulkModuleApplicationContractsModule),
    typeof(AbpHttpClientModule))]
public class BulkModuleHttpApiClientModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddHttpClientProxies(
            typeof(BulkModuleApplicationContractsModule).Assembly,
            BulkModuleRemoteServiceConsts.RemoteServiceName
        );

        Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<BulkModuleHttpApiClientModule>();
        });

    }
}
