﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace MyComp.BulkModule.Entities
{
    public class TableA : Entity<Guid>
    {

        public TableA(){}
        
        public TableA(Guid id, int code, string name, string desc = null)
        {
            Id = id;
            Code = code;
            Name = name;
            Desc = desc;
        }

        /// <summary>
        /// 批处理需要Id属性赋值，不能在构造中赋值，而基类中set方法是protected的，因此此处需要new关键字覆盖默认的Id属性
        /// </summary>
        public new Guid Id { get; set; }

        public int Code { get; set; }

        public string Name { get; set; }

        public string Desc { get; set; }
    }
}
