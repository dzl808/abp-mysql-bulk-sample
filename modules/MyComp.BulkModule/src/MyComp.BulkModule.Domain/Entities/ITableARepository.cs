﻿using System;
using Volo.Abp.Domain.Repositories;

namespace MyComp.BulkModule.Entities
{
    public interface ITableARepository : IBasicRepository<TableA, Guid>
    {
    }
}
