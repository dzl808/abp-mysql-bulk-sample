﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace MyComp.BulkModule.Entities
{
    public interface ITableABulkRepository
    {
        Task UpsertManyAsync(IEnumerable<TableA> members, CancellationToken cancellationToken = default);
    }
}
