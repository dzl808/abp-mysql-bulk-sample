﻿using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace MyComp.BulkModule;

[DependsOn(
    typeof(AbpDddDomainModule),
    typeof(BulkModuleDomainSharedModule)
)]
public class BulkModuleDomainModule : AbpModule
{

}
