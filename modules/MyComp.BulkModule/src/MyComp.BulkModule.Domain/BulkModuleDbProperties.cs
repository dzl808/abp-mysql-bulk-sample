﻿namespace MyComp.BulkModule;

public static class BulkModuleDbProperties
{
    public static string DbTablePrefix { get; set; } = "BulkModule";

    public static string DbSchema { get; set; } = null;

    public const string ConnectionStringName = "BulkModule";
}
