﻿using Volo.Abp.Settings;

namespace MyComp.BulkModule.Settings;

public class BulkModuleSettingDefinitionProvider : SettingDefinitionProvider
{
    public override void Define(ISettingDefinitionContext context)
    {
        /* Define module settings here.
         * Use names from BulkModuleSettings class.
         */
    }
}
