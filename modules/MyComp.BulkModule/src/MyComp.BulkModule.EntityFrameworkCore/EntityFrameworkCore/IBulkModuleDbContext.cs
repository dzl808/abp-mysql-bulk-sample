﻿using Microsoft.EntityFrameworkCore;
using MyComp.BulkModule.Entities;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace MyComp.BulkModule.EntityFrameworkCore;

[ConnectionStringName(BulkModuleDbProperties.ConnectionStringName)]
public interface IBulkModuleDbContext : IEfCoreDbContext
{
    DbSet<TableA> TableA { get; }
}
