﻿using Microsoft.EntityFrameworkCore;
using MyComp.BulkModule.Entities;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace MyComp.BulkModule.EntityFrameworkCore;

public static class BulkModuleDbContextModelCreatingExtensions
{
    public static void ConfigureBulkModule(
        this ModelBuilder builder)
    {
        Check.NotNull(builder, nameof(builder));

        builder.Entity<TableA>(b =>
        {
            //Configure table & schema name
            b.ToTable(BulkModuleDbProperties.DbTablePrefix + "TableA", BulkModuleDbProperties.DbSchema);

            b.ConfigureByConvention();

            //Properties
            b.Property(q => q.Name).HasMaxLength(64);
            b.Property(q => q.Desc).HasMaxLength(256);

        });
    }
}
