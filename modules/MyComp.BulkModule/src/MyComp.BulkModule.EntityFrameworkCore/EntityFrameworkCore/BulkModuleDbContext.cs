﻿using Microsoft.EntityFrameworkCore;
using MyComp.BulkModule.Entities;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace MyComp.BulkModule.EntityFrameworkCore;

[ConnectionStringName(BulkModuleDbProperties.ConnectionStringName)]
public class BulkModuleDbContext : AbpDbContext<BulkModuleDbContext>, IBulkModuleDbContext
{
    public DbSet<TableA> TableA { get; set; }

    public BulkModuleDbContext(DbContextOptions<BulkModuleDbContext> options)
        : base(options)
    {

    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.ConfigureBulkModule();
    }
}
