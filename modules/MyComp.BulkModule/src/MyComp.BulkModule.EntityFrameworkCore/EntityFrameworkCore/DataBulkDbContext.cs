﻿using Microsoft.EntityFrameworkCore;
using MyComp.BulkModule.Entities;

namespace MyComp.BulkModule.EntityFrameworkCore
{
    public class DataBulkDbContext : DbContext
    {
        public DbSet<TableA> TableA { get; set; }

        public DataBulkDbContext(DbContextOptions<DataBulkDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<TableA>(b =>
            {
                //Configure table & schema name
                b.ToTable(BulkModuleDbProperties.DbTablePrefix + "TableA", BulkModuleDbProperties.DbSchema);

                // 显示设置主键
                b.HasKey(b => b.Id);
                // 批处理扩展包中要求主键值类型为ValueGeneratedNever，默认为ValueGeneratedOnAdd，会报错
                b.Property(t => t.Id).ValueGeneratedNever();

                // 如果有扩展字段，忽略掉
                //b.Ignore(b => b.ExtraProperties);

            });
        }
    }
}
