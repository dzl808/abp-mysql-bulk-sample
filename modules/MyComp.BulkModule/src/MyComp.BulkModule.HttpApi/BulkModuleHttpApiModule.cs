﻿using Localization.Resources.AbpUi;
using MyComp.BulkModule.Localization;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;

namespace MyComp.BulkModule;

[DependsOn(
    typeof(BulkModuleApplicationContractsModule),
    typeof(AbpAspNetCoreMvcModule))]
public class BulkModuleHttpApiModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        PreConfigure<IMvcBuilder>(mvcBuilder =>
        {
            mvcBuilder.AddApplicationPartIfNotExists(typeof(BulkModuleHttpApiModule).Assembly);
        });
    }

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Get<BulkModuleResource>()
                .AddBaseTypes(typeof(AbpUiResource));
        });
    }
}
