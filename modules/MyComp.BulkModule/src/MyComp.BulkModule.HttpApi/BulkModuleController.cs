﻿using MyComp.BulkModule.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace MyComp.BulkModule;

public abstract class BulkModuleController : AbpControllerBase
{
    protected BulkModuleController()
    {
        LocalizationResource = typeof(BulkModuleResource);
    }
}
