﻿using Volo.Abp.Localization;

namespace MyComp.BulkModule.Localization;

[LocalizationResourceName("BulkModule")]
public class BulkModuleResource
{

}
