﻿using Volo.Abp.Application;
using Volo.Abp.Modularity;
using Volo.Abp.Authorization;

namespace MyComp.BulkModule;

[DependsOn(
    typeof(BulkModuleDomainSharedModule),
    typeof(AbpDddApplicationContractsModule),
    typeof(AbpAuthorizationModule)
    )]
public class BulkModuleApplicationContractsModule : AbpModule
{

}
