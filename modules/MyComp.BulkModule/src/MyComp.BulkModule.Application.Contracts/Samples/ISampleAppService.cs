﻿using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace MyComp.BulkModule.Samples;

public interface ISampleAppService : IApplicationService
{
    Task<SampleDto> GetAsync();

    Task<SampleDto> GetAuthorizedAsync();

    /// <summary>
    /// abp常规插入
    /// </summary>
    /// <returns></returns>
    Task InsertManyAsync();
    /// <summary>
    /// 批量插入或更新
    /// </summary>
    /// <returns></returns>
    Task BulkUpsertAsync();
}
