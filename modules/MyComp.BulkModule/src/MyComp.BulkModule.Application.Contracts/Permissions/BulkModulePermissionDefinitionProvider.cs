﻿using MyComp.BulkModule.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace MyComp.BulkModule.Permissions;

public class BulkModulePermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var myGroup = context.AddGroup(BulkModulePermissions.GroupName, L("Permission:BulkModule"));
    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<BulkModuleResource>(name);
    }
}
