﻿using Volo.Abp.Reflection;

namespace MyComp.BulkModule.Permissions;

public class BulkModulePermissions
{
    public const string GroupName = "BulkModule";

    public static string[] GetAll()
    {
        return ReflectionHelper.GetPublicConstantsRecursively(typeof(BulkModulePermissions));
    }
}
