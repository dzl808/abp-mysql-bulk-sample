﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using MyComp.BulkModule.Entities;
using Volo.Abp.Uow;

namespace MyComp.BulkModule.Samples;

public class SampleAppService : BulkModuleAppService, ISampleAppService
{

    private ITableARepository _tableARepository;
    private ITableABulkRepository _tableABulkRepository;
    public SampleAppService(ITableARepository tableARepository, ITableABulkRepository tableABulkRepository)
    {
        _tableARepository = tableARepository;
        _tableABulkRepository = tableABulkRepository;
    }

    public async Task InsertManyAsync()
    {
        var data = new List<TableA>();

        for(int i = 0; i< 20000; i++)
        {
            data.Add(new TableA(GuidGenerator.Create(), i, $"Test{i}"));
        }

        await _tableARepository.InsertManyAsync(data);

    }

    public async Task BulkUpsertAsync()
    {
        var data = new List<TableA>();

        for (int i = 0; i < 20000; i++)
        {
            data.Add(new TableA(GuidGenerator.Create(), i, $"Test{i}"));
        }

        await _tableABulkRepository.UpsertManyAsync(data);
    }

    public Task<SampleDto> GetAsync()
    {
        return Task.FromResult(
            new SampleDto
            {
                Value = 42
            }
        );
    }

    [Authorize]
    public Task<SampleDto> GetAuthorizedAsync()
    {
        return Task.FromResult(
            new SampleDto
            {
                Value = 42
            }
        );
    }

}
