﻿using AutoMapper;

namespace MyComp.BulkModule;

public class BulkModuleApplicationAutoMapperProfile : Profile
{
    public BulkModuleApplicationAutoMapperProfile()
    {
        /* You can configure your AutoMapper mapping configuration here.
         * Alternatively, you can split your mapping configurations
         * into multiple profile classes for a better organization. */
    }
}
