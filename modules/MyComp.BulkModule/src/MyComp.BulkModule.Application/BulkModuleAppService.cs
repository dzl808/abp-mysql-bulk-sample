﻿using MyComp.BulkModule.Localization;
using Volo.Abp.Application.Services;

namespace MyComp.BulkModule;

public abstract class BulkModuleAppService : ApplicationService
{
    protected BulkModuleAppService()
    {
        LocalizationResource = typeof(BulkModuleResource);
        ObjectMapperContext = typeof(BulkModuleApplicationModule);
    }
}
