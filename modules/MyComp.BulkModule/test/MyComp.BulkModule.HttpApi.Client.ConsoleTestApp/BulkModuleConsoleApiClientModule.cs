﻿using Volo.Abp.Autofac;
using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace MyComp.BulkModule;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(BulkModuleHttpApiClientModule),
    typeof(AbpHttpClientIdentityModelModule)
    )]
public class BulkModuleConsoleApiClientModule : AbpModule
{

}
