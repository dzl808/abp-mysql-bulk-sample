﻿using Volo.Abp.Modularity;

namespace MyComp.BulkModule;

[DependsOn(
    typeof(BulkModuleApplicationModule),
    typeof(BulkModuleDomainTestModule)
    )]
public class BulkModuleApplicationTestModule : AbpModule
{

}
